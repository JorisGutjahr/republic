pragma solidity ^0.5.10;

contract Identification {
            /*
            Identification:
            The ID of a citizen of the Republic.
            Fields in the ID:
            string name  : Name of the citizen
            string first_name : First name of the citizen
            uint birthday :  Year of birth of the citizen ( YYYY )
            string lang : The languages, that the citizen speaks ( de, en, fr, es, etc. )
            uint expiry : Date of expiry, timestamp at which a new Identification is required.
            address authority : authority / other citizen, that created this Identification.
            address account : Ethereum Account of the citizen.
            */
    string name;                                // Name of the citizen
    string first_name;                        //  First name of the citizen
    uint birthday;                              //  Year of birth of the citizen ( YYYY )
    string lang;                                // Primary languages of the citizen
    uint expiry;                                //  Date of expiry, timestamp at which a new Identification is required
    address authority;                      // Authoriy / other citizen, that created this Identification, every citizen has the right to create an Identification for another citizen.
    address account;                       // account of the citizen
    constructor(
        string _name, 
        string _first_name, 
        uint _birthday, 
        string _lang,
        uint _expiry,
        address _account
        ) public {

            
    }
}